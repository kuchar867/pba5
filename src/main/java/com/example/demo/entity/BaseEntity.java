package com.example.demo.entity;

import java.io.Serializable;

/**
 * @author jakub
 * 15.04.2023
 */
public interface BaseEntity<ID extends Serializable> {
    ID getId();

    void setId(ID id);
}
